---
layout: temoignage
nom: Codeurs en Liberté
homepage: https://www.codeursenliberte.fr
date: 2020-01-02
---

## Qui sommes-nous ?

Nous sommes une coopérative de travailleur·euses du numérique indépendant·es. Créée en 2015, la structure repose sur quelques idées fondatrices.

### Le statut de salarié·e.

Choisir le temps de travail et les missions nous semble indispensable. Cela est à première vue incompatible avec un statut de salarié·e. Cependant, la protection sociale des indépendant·es est compliquée à obtenir, et ne nous semble pas offrir autant de garanties que le régime général.

En nous salariant dans une structure commune, nous pouvons être affilié·es au régime général, tout en gardant la liberté de notre temps de travail et de nos missions.

### Le choix du temps de travail.

Codeur·se en Liberté attache une grande importance au travail fait avec amour. Ainsi, chacun·e peut travailler moins pour travailler mieux pour garantir la qualité de ses contributions et un équilibre avec sa vie privée.

Par exemple, nous encourageons un maximum de 4 jours travaillés par semaine. C’est également pour cela que notre mode de rémunération sépare les comptes de chaque sociétaire : l’objectif est que personne ne se sente redevable à la coopérative, l’amenant à travailler au-delà de ses besoins.

### Plus solidaires que solitaires.

Travailler sans chef, c’est génial – mais travailler seul est un peu usant à la longue. Au delà d’une simple structure administrative, Codeurs en Liberté est aussi un espace pour travailler ensemble, s’entraider, partager des connaissances et des GIF animés, et se serrer les coudes en cas de coup dur.

Par ailleurs, les sociétaires contribuent financièrement à la coopérative en fonction de leurs revenus, avec un système progressif et une forme de revenu de base. Cette solidarité financière rend notre mode de fonctionnement à la fois viable pour les hauts salaires, et compétitif pour les sociétaires qui facturent peu (et qui ainsi ne perdent pas à avoir un statut de salarié·e plutôt que d’auto-entrepreneur).

### Un fonctionnement militant.

Nous nous efforçons que chaque membre s’approprie l’entreprise. Cela passe par un actionnariat obligatoire et — en reprenant les principes des coopératives — la possession d’une voix quel que soit le montant de capital détenu.

C’est aussi pour cela que, au lieu de payer quelqu’un pour les fonctions transverses, nous préférons l'auto-gestion. À la fin du mois, chaque salarié·e est responsable d’établir sa paie, de se payer son salaire, et de participer aux tâches comptables et administratives collectives.

### Partager nos connaissances.

Nous sommes convaincus que la transparence doit être l’approche par défaut et qu’elle est nécessaire pour permettre l’implication réelle de chaque membre. Nous poussons cette transparence en rendant publique la majeure partie de notre activité. Seules les négociations commerciales et les données à caractère personnel ne sont pas publiées.

Par exemple, les décisions de nos assemblées générales, ou encore les échanges sur divers points de fonctionnement (et pas que le résultat final) peuvent être consultés sur <https://gitlab.com/CodeursEnLiberte/fondations/>.

Nous essayons de faire un grand effort pour documenter nos réalisations et permettre leur réutilisation. Par ailleurs, malgré des membres et des clients aux langues maternelles variées, nous nous efforçons de ne pas retomber systématiquement sur l’anglais comme choix par défaut.

## Vie de tous les jours

Tous membres sont indépendants et travaillent dans des endroits différents, nous n’avons pas de local où nous retrouver physiquement régulièrement. Nous avons donc quelques outils pour nous permettre de communiquer et prendre des décisions :
 - Mattermost : un outil de chat auto-hébergé ;
 - GitLab : on s’en sert beaucoup pour discuter par écrit, et garder un historique sur toutes les décisions que nous prenons. On l’utilise aussi pour stocker certains documents administratifs (et bien sûr à versionner nos outils internes) ;
- Des tours de table réguliers : une fois par semaine, il y a un appel collectif où chaque personne peut partager ce qu’elle a fait dans la semaine, et où l’on parle des sujets en cours ;
 - Des séminaires : une fois par an, nous nous retrouvons au même endroit pendant pendant une semaine, pour passer du temps ensemble et avancer sur les sujets de la coopérative.

## Répartition des revenus

Chaque membre de la coopérative est indépendant et a donc la liberté de gérer son temps de travail comme il l’entend. Cette liberté s’applique aussi à la rémunération : chaque personne se rémunère en fonction de ce qu’elle a facturé.
Concrètement, quand un membre facture un client, une partie va dans la coopérative, l’autre partie dans un compte virtuel personnel. Il se verse alors un salaire grâce à ce compte.

Il y a des limites à ce mode de répartition : il est difficile pour des petits salaires de supporter à la fois les cotisations sociales et cette contribution au fonctionnement de la coopérative.

Nous avons donc mis en place un reversement pour les bas salaires : en dessous d’un certain seuil, la coopérative reverse une contribution au membre. Cela permet d’être compétitif avec le statut d’auto-entrepreneur pour les revenus en bas de l’échelle.

_Nous gérons les comptes virtuels grâce à [cet outil](https://gitlab.com/CodeursEnLiberte/ardoise)._

## Recrutement

Nous recrutons sur cooptation d’un·e des membres de la coopérative. Nous demandons ensuite à la personne de parler avec chacun des membres de la coopérative ; puis, s’il n’y a pas de véto, nous entamons les démarches administratives pour employer la personne en CDI.

Cela dit, notre but est moins de grossir que d’essaimer. Rien ne nous fait plus plaisir que la création de nouvelles coopératives. Tout [notre mode de fonctionnement et nos manuels pratiques](https://gitlab.com/CodeursEnLiberte/fondations/-/wikis/home) sont documentés et disponibles : si vous êtes intéressés par la création d’une structure similaire, forkez-nous.
