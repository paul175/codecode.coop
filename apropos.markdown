---
layout: default
title:  "À propos"
date:   2019-10-10 17:20:00 +0200
categories: main
---

**[codecode.coop](https://codecode.coop)** est une tentative de contribuer au développement du modèle coopératif dans le milieu informatique.

C’est un projet expérimental, encore en cours de réalisation. Pour l’instant, il comporte essentiellement une traduction du « [A Technology Freelancer’s Guide To Starting a Worker Cooperative](https://institute.coop/resources/technology-freelancers-guide-starting-worker-cooperative) », publié par le _Network of American Tech Worker Cooperatives_ (NATWC) en 2009.

À terme, nous espérons y ajouter :

- Plus de guides technique (rédaction de statuts, contrats, etc.) ;
- Des témoignages de coopératives informatiques déjà existantes ;
- Toute contribution auxquelles nous n'aurions pas pensé pour l'instant.

Ce site a une vocation collaborative. Si le cœur vous en dit, ou que vous voudriez ajouter votre témoignage, venez donc [discuter et contribuer sur GitLab](https://gitlab.com/CodeursEnLiberte/codecode.coop) !
