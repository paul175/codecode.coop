Code Code Coop promeut le développement de coopératives dans le milieu de l’informatique.

## Installation en local

Pour installer le site sur votre ordinateur et le lancer :

1. Installer un [environnement Ruby](https://github.com/rbenv/rbenv#installation) ;
2. `make run`

## Déploiement

Le site est hébergé sur GitLab Pages.

Les modifications mergées dans la branche `main` sont automatiquement déployées sur https://codecode.coop par un job d’Intégration Continue.
